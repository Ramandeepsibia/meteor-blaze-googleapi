/** 
* This js file contains the helpers and events for to assist 
* welcome.html template 
* Assists drievrs and riders render different screens based on their user type
**/


Template.welcome.helpers({
		username:function(){
			if(Meteor.user()){
				return Meteor.user().profile.name;
			//return Meteor.user().profile.car[0].vin;

			//return Meteor.user().emails[0].address;
		}
		else{
			return "anonymous internet user??";
		}
	},

	//returns true if the user is a driver
	userType:function(){
		if(Meteor.user()){
			return Meteor.user().profile.type==="driver";
		}
	}
});

Template.welcome.events({
	'click .logout': function(event){
	event.preventDefault();
	console.log("logged out");   
    var user_obj = Meteor.users.findOne({_id:Meteor.userId()});
	var location_object = userLocations.findOne({userId:Meteor.userId()});

	if((user_obj.profile.type==="driver")&&(location_object!=null)&&(location_object.OnlineStatus===true))
		{
			console.log("logged out: "+user_obj.profile.type);
			userLocations.update({_id: location_object._id}, {$set: {"OnlineStatus":false}});                
        }
	
	Session.clear();
   		Meteor.logout();
	Router.go('/');
    	}    
});


