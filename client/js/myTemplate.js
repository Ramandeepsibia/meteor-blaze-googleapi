/** 
* This js file contains the helpers and events for to assist 
* myTemplate.html template 
* Assists riders gets notifications when a driver has accepted their request
**/
var driverDetails= new Array();
var driverDetailsDest = new Array();
var driverName;
var carRego;

Template.myTemplate.onRendered(function () {
    this.autorun(() => {
 
    var lat1 = Session.get('riderlat');
    var lng1 =  Session.get('riderlng');
    var driverId = Session.get('driver_id');
    console.log("a try to get driver id " +  driverId);
    //console.log("got current loc");
    //Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.currLocation": loc}});
             
    //lat1 = loc.lat;

    //ng1 = loc.lng;

    
    //  var user_obj = Meteor.users.findOne({_id:});
    //console.log(Meteor.users.findOne({_id:Session.get('driver_id')}));
    // Getting the name of the driver
    // driverName = user_obj.profile.name;
    driverName = Session.get('driverName');
    carRego = Session.get('carRego');
    //Meteor.users.find( { 'points.0.points': { $lte: 55 } } )
    console.log("My drivers car is  " + carRego);
      
    //Getting the location co-ordinates of the driver 
    var driverLocCords = userLocations.findOne({userId:driverId});

    //Getting riders location co-ordinates
    var riderLocCords = userLocations.findOne({userId:Meteor.userId()});

    // getting the distance among them in kms hence divide by 1000
    distance=geolib.getDistance(    
      { latitude: riderLocCords.location.lat, longitude: riderLocCords.location.lng },
          { latitude: driverLocCords.location.lat, longitude: driverLocCords.location.lng }
      );
    distance=distance/1000;
    console.log("distance in kms between rider and driver: "+distance);

    /**
    * Using the directions service to find out the time of travel between two
    **/

    var origin = driverLocCords.location.lat+ ', ' + driverLocCords.location.lng; // using google.maps.LatLng class
          
    var destination = riderLocCords.location.lat + ', ' + riderLocCords.location.lng; // using string
          

    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: origin, // LatLng|string
        destination: destination, // LatLng|string
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };

    /**
    * calculating the time to travel for the driver
    **/    
    var directionsDisplay;
    directionsService.route( request, function( response, status ) {

      if ( status === 'OK' ) {
          var point = response.routes[ 0 ].legs[ 0 ];
          console.log("est--> "+point.duration.text+"  and  "+point.distance.text);
          Session.set("travel_time",point.duration.text);
          Session.set("travel_dist",point.distance.text);
      }

    });



      /**
      * Push the details of the trip request to an array
      **/

      if(Session.get('travel_dist')){
        driverDetails = [];
        driverDetails.push({
        driverName: driverName,
        travel_time: Session.get('travel_time'), 
        travel_dist: Session.get('travel_dist'),
        carRego: carRego
      });
    
        //  });//for each

        /**
        * Setting the array in the session
        **/
        Session.set("driverDetails"," ");
        Session.set("driverDetails",driverDetails);
        }
      


      /**
      * details after the trip has started
      *
      **/
      // getting the distance among them in kms
    distance=geolib.getDistance(    
      { latitude: riderLocCords.location_destination.lat, longitude: riderLocCords.location_destination.lng },
          { latitude: driverLocCords.location.lat, longitude: driverLocCords.location.lng }
      );
    distance=distance/1000;
    console.log("distance in kms between rider and driver: "+distance);

    /**
    * Using the directions service to find out the time of travel between two
    **/

    var origin = driverLocCords.location.lat+ ', ' + driverLocCords.location.lng; // using google.maps.LatLng class
          
    var destination = riderLocCords.location_destination.lat + ', ' + riderLocCords.location_destination.lng; // using string
          

    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: origin, // LatLng|string
        destination: destination, // LatLng|string
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };


    /**
          * calculating the time to travel for the driver
          **/    
    var directionsDisplay;
    directionsService.route( request, function( response, status ) {

        if ( status === 'OK' ) {
            var point = response.routes[ 0 ].legs[ 0 ];
            console.log("est--> "+point.duration.text+"  and  "+point.distance.text);
            Session.set("travel_time_dest",point.duration.text);
            Session.set("travel_dist_dest",point.distance.text);
              }

    });



      /**
      * Push the details of the trip request to an array
      **/

      if(Session.get('travel_time_dest')){
        driverDetailsDest = [];
        driverDetailsDest.push({
        driverName: driverName,
        travel_time_dest: Session.get('travel_time_dest'), 
        travel_dist_dest: Session.get('travel_dist_dest'),
        carRego: carRego
        });
    
        //  });//for each

        /**
        * Setting the array in the session
        **/
        Session.set("driverDetailsDest","");
        Session.set("driverDetailsDest",driverDetailsDest);
        }
  
        $(".afterPickupScreen").hide();
        

        var count = 0;
        var tripID = Session.get("tripID");
        Session.set("idOfTrip", tripID);
        Trips.find({_id :tripID}).observe({

            changed:function(new_document, old_document){
                //console.log('trips observe changed value function');
                       
                count++;

                //console.log("count is became-->> " + count);

                Session.set('pickupMsg', "Driver has reached at the pick up");
                if (count == 1){
                    swal({   title: "Your Driver has reached at the pick up location",   
                        text: "Please be there!!",   
                        timer: 60000,   
                        showConfirmButton: true 
                    });

                    $(".beforePickupScreenDist").hide();
                }
                else if(count == 2){
                    Session.set('pickupMsg', "Trip has started");
                    swal({   title: "Trip has started",   
                        text: "Have a nice journey",   
                        timer: 60000,   
                        showConfirmButton: true 
                    });

                    $(".beforePickupScreen").hide();
                    $(".afterPickupScreen").show();

                }
                else if(count == 3){
                    Session.set('pickupMsg', "Trip has ended");
                    swal({   title: "Trip has ended & you saved 4 kg of CO2",   
                        text: "How was the experience?",   
                        timer: 60000,   
                        showConfirmButton: true 
                    });
                    Session.set("RequestSubmitted", false); 
                    count = 0;
                   

                    Router.go('/rateTrip');

                }
                else{
                     Session.set('pickupMsg', "A Driver is on your way");

                }


                 
            }
        });

    });

});



Template.myTemplate.helpers({

    /**
    * Returns the new rider requests to the driver to see
    **/
    driverDetails: function () {         
    
    if(Session.get("driverDetails"))
      {
        return Session.get("driverDetails");
      }

    } ,

    driverDetailsDest: function (){

      if(Session.get("driverDetailsDest"))
      {
        return Session.get("driverDetailsDest");
      }
    },

    msg: function(){
      var pickupMsg = Session.get("pickupMsg");
      return pickupMsg;
    },
   
    pathGoogleMapsDriverLoc: function(){

      var tripID = Session.get("tripID");

      //console.log(tripID);
      var tripObj=Trips.findOne({_id:tripID}); 

      //getting source coords of rider   
      userLocationsObj_rider = userLocations.findOne({userId:tripObj.rider_id});
      var lat_rider = userLocationsObj_rider.location.lat;
      var lng_rider = userLocationsObj_rider.location.lng;

      /**
       *Location address of rider
       **/
        
       reverseGeocode.getLocation(lat_rider, lng_rider, function(l){

          // Session.set('location', reverseGeocode.getAddrStr());
          var riderSourceAddress = reverseGeocode.getAddrStr();
          
          //console.log("riderSourceAddress" + riderSourceAddress);
           Session.set('riderSourceAddress', riderSourceAddress);
        });

       //getting source coords of driver 
       userLocationsObj_driver = userLocations.findOne({userId:tripObj.driver_id});
       var lat_driver = userLocationsObj_driver.location.lat;
       var lng_driver = userLocationsObj_driver.location.lng;

       /**
       *Location address of driver
       **/

       reverseGeocode.getLocation(lat_driver, lng_driver, function(l){

         // Session.set('location', reverseGeocode.getAddrStr());
          var driverSourceAddress = reverseGeocode.getAddrStr();
          //console.log("driverSourceAddress" + driverSourceAddress);
          Session.set('driverSourceAddress', driverSourceAddress);

        });

      // console.log(lng_driver);

       
      Session.set('lat_driver', lat_driver);
      Session.set('lng_driver', lng_driver);


      var origin ;
      if (Session.get('lat_driver') != null && Session.get('lat_driver') != null) {
          origin= Session.get('lat_driver') + "," +Session.get('lng_driver');
      }

     // console.log(origin);
      /**
      * returns the url with options for rider side map
      **/

      var path = "";
      if ( origin != undefined && origin != null ) {
          // When have the origen defined ,144.7977140
          var destination = lat_rider + "," + lng_rider;
          console.log(origin);
          path = "https://www.google.com/maps/embed/v1/directions"
              + "?key=AIzaSyAXFzehVMiKB2yNIeI467aeoBUBrbg1H5Q";
          path+= "&origin=" + origin;
          path+=  "&destination="+ destination;
          path+=  "&avoid=tolls|highways";
          //console.log(path);
      } else {
          // When I don't have the origen and destination
          path = "https://www.google.com/maps/embed/v1/place"
              + "?key=AIzaSyAXFzehVMiKB2yNIeI467aeoBUBrbg1H5Q";
          path+= "&q=" + this.street + "," + this.addressNumber + "," + this.neighborhood + "," + this.city ;
          path+= "&zoom=14&maptype=roadmap";
          //console.log(path);
      }
      return path;
    }

});