/** 
* This js file contains the helpers and events for to assist 
* settingsPage.html template 
* Assists users change their account details 
* Drivers can change their car details 
* FILE NOT IN USE NOW
**/

Template.settingsPage.helpers({
    /**
    * returns username 
    **/
    username:function(){
    if(Meteor.user()){
      return Meteor.user().profile.name;
    }

    else{
      return "anonymous internet user??";
    }

  },

    /**
    * returns regNo 
    **/
    regNumber:function(){
    if(Meteor.user()){
      return Meteor.user().profile.regNumber;
    }

    else{
      return "anonymous regNumber??";
    }

  },

      /**
    * returns license 
    **/
    license:function(){
    if(Meteor.user()){
      return Meteor.user().profile.license;
    }

    else{
      return "anonymous license??";
    }

  },

      /**
    * returns model 
    **/
    model:function(){
    if(Meteor.user()){
      return Meteor.user().profile.model;
    }

    else{
      return "anonymous model??";
    }

  },

      /**
    * returns brand 
    **/
    brand:function(){
    if(Meteor.user()){
      return Meteor.user().profile.brand;
    }

    else{
      return "anonymous brand??";
    }

  },

      /**
    * returns gender Preference 
    **/
    gender:function(){
    if(Meteor.user()){
      return Meteor.user().profile.genderPreference;
    }

    else{
      return "anonymous genderPreference??";
    }

  },

      /**
    * returns capacity 
    **/
    capacity:function(){
    if(Meteor.user()){
      return Meteor.user().profile.capacity;
    }

    else{
      return "anonymous capacity??";
    }

  },

      /**
    * returns campus 
    **/
    campus:function(){
    if(Meteor.user()){
      return Meteor.user().profile.campus;
    }

    else{
      return "anonymous campus??";
    }

  },


/**
  * returns type of user driver or rider
  **/
  userType:function(){
    if(Meteor.user().profile.type == "driver"){
      //console.log();
      return true;
    }
      else{ return false;}
    
    
  },

    isBoth:function(){
    if(Meteor.user().profile.isBothTypes == true){
      //console.log();
      return true;
    }
      else{ return false;}
    
    
  },

  /**
  * retuns email of user
  **/
  email:function(){
    if(Meteor.user()){
      return Meteor.user().emails[0].address;
    }
    
  },
  /**
  * retuns photo of user
  **/
  photo: function () {
    return Session.get("photo");
  },


});

Session.setDefault('img', null);

  var getPicture = function(opts) {
    MeteorCamera.getPicture(opts, function(err, data) {
      if (err) {
        console.log('error', err);
      }
      if (data) {
        Session.set('img', data)
      }
    });
  };

  Template.settingsPage.events({
  'submit .form-horizontal'(event) {
    // Prevent default browser form submit
    //var conf = true;

    event.preventDefault();
    swal("Success!", "Your details have been updated.", "success",);
      var updatedName=$('[name=updatedName]').val();
      var updatedEmail=$('[name=updatedEmail]').val();
      var newVin = $('[name=newVin]').val();
      var gender = $('[name=gender]').val();
      var campus=$('[name="campus"]').val();

    

    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.name": updatedName}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"emails.0.address": updatedEmail}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.regNumber": newVin}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.genderPreference": gender}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.campus": campus}});

    console.log("campus got is:   "+campus);
    var lat =0.0;
    var lng= 0.0;
    if(campus==="Latrobe University, Melbourne Campus")
    {
      lat=-37.72076119999999;
      lng=145.04709360000004;
    }
    else if(campus==="LaTrobe University, City Campus")
    {
      lat=-37.816327;
      lng=144.962338;
    }
      else if(campus==="LaTrobe University, Bendigo Campus")
    {
      lat=-36.783737;
      lng=144.303075;
    } else if(campus==="LaTrobe University, Albury-Wodonga campus")
    {
      lat=-36.106552;
      lng=146.835499;
    } else if(campus==="LaTrobe University, Franklin Street Campus")
    {
      lat=-37.809767;
      lng=144.957699;
    } else if(campus==="LaTrobe University, Mildura Campus")
    {
      lat=-34.268104;
      lng=142.091720;
          
    } else if(campus==="LaTrobe University, Shepparton Campus")
    {
      lat=-36.379912;
      lng=145.406730;
    } else if(campus==="LaTrobe University, Sydney Campus")
    {
      lat=-33.875712;
      lng=151.209204;
    } 
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.campus_lat": lat}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.campus_lng": lng}});


    },


      'click #updateCar'(event) {
    // Prevent default browser form submit
    //var conf = true;

    event.preventDefault();
    swal("Success!", "Your car details have been updated!.", "success",);

      var license =$('[name=license]').val();
      var regNumber=$('[name=regNumber]').val();
      var gender = $('[name=gender]').val();
      var model=$('[name="model"]').val();
      var brand=$('[name="brand"]').val();
      var capacity=$('[name="capacity"]').val();

    

    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.license": license}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.regNumber": regNumber}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.genderPreference": gender}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.brand": brand}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.model": model}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.capacity": capacity}});






    },



   'click #becomeDriver':function(event){

    event.preventDefault();
    Router.go('becomeDriver');


    }, 

    'click #switchToRider':function(event){
    event.preventDefault();
    var type = "rider";
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.type": type}});

    Router.go('welcome');

    }, 

    'click #switchToDriver':function(event){
    event.preventDefault();
    var type = "driver";
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.type": type}});

    Router.go('welcome');

    }, 



    'click #logout': function(event){
    //event.preventDefault();
    console.log("logged out-->");   
    var user_obj = Meteor.users.findOne({_id:Meteor.userId()});
    console.log("user obj: "+user_obj);
    var location_object = userLocations.findOne({userId:Meteor.userId()});
    console.log("location_object: "+location_object);
    if(user_obj.profile){
      if((user_obj.profile.type==="driver")&&(location_object!=null)&&(location_object.OnlineStatus===true))
      {
         console.log("logged out: "+user_obj.profile.type);
         userLocations.update({_id: location_object._id}, {$set: {
          "OnlineStatus":false
          }
        });                
      }
    }
    

    Session.clear();
    Meteor.logout(); 

    Router.go('/');
    
    Modal.hide('settingsModal');

   
  
  }    



});

      Template.cameraEvent.events({
  'click button': function () {
    getPicture({
      width: 350,
      height: 100,
      quality: 75
    });
  }
});

Template.libraryEvent.events({
  'click button': function () {
    if (Meteor.isCordova) {
      getPicture({
        width: 350,
        height: 100,
        quality: 75,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
      });
    } else {
      alert('Cordova only feature.');
    }
  }
});

Template.img.helpers({
  img: function() {
    return Session.get('img');
  }
});