/** 
* This js file contains the helpers for 
* helpModal.html template 
**/

Template.helpModal.helpers({
		//returns true if the user is a driver
	userType:function(){
		if(Meteor.user()){
			if(Meteor.user().profile.type== "driver")
			{
				return true;
			}
			else if(Meteor.user().profile.type== "rider")
			{
				return false;
			}
		}
	}

})