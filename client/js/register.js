/** 
* This js file contains the helpers and events for to assist 
* register.html template 
* Assists users register to the app
**/

Template.register.helpers({
	validation: function () {					
		return Session.get("showDriverDetails");
	}

});


Template.register.events({
    	'submit form': function(event){
			event.preventDefault();
        },
    	
       'click #driver': function(e){
	        console.log("I am clicked");
	        //Router.go('driverDetails');
	        Session.set("showDriverDetails",true);
		},

       'click #rider': function(e){
	        console.log("I am clicked");
	        //Router.go('driverDetails');
	        Session.set("showDriverDetails",false);
		},
        'click #signUpModaldh': function(e) {
		    e.preventDefault();

		    Router.go('/');
		},
		
		'click #termsModal': function(e) {
		   e.preventDefault();

   			 Modal.show('termsModal');
		}

   });

	//This method takes information from the registeration form and creates the users 
	Template.register.onRendered(function(){
		Session.set('vin', false);
		
    	var validator = $('.register').validate({
			submitHandler: function(event){
           	console.log("you have just registered");
		 
			var email = $('[name=email]').val();
        	var password = $('[name=password]').val();
        	var campus=$('[name="campus"]').val();
			var name=$('[name=name]').val();
			var vin =$('[name=vin]').val();
			var type=$('input[name="usertype"]:checked').val();    

			var license=$('[name="license"]').val();
			var regNumber=$('[name="regNumber"]').val();
			var brand=$('[name="brand"]').val();
			var model=$('[name="model"]').val();
			var capacity=$('[name="capacity"]').val();
			var isBothTypes = false;
			if(type=='driver')
			{
				isBothTypes=true;
			}

			/**
			* validation field for terms of use
			**/

			var terms=$('input[name="termsOfUse"]:checked').val(); 

			console.log("brand: "+brand);
		
			
			//var currLocation=null;
			//var currLocation_str=null;
			//var pickUp=null;   //rider's field, would be updated from raman's code
			//var goOnline=false;
			//var sendRequest=true;  //temporary true, will be assigned true in raman's code
             var lat=0.0;
			 var lng=0.0;
			
			 if(campus==="Latrobe University, Melbourne Campus")
			    {
			      lat=-37.72076119999999;
			      lng=145.04709360000004;
			    }
			    else if(campus==="LaTrobe University, City Campus")
			    {
			      lat=-37.816327;
			      lng=144.962338;
			    }
			      else if(campus==="LaTrobe University, Bendigo Campus")
			    {
			      lat=-36.783737;
			      lng=144.303075;
			    } else if(campus==="LaTrobe University, Albury-Wodonga campus")
			    {
			      lat=-36.106552;
			      lng=146.835499;
			    } else if(campus==="LaTrobe University, Franklin Street Campus")
			    {
			      lat=-37.809767;
			      lng=144.957699;
			    } else if(campus==="LaTrobe University, Mildura Campus")
			    {
			      lat=-34.268104;
			      lng=142.091720;
				  
			    } else if(campus==="LaTrobe University, Shepparton Campus")
			    {
			      lat=-36.379912;
			      lng=145.406730;
			    } else if(campus==="LaTrobe University, Sydney Campus")
			    {
			      lat=-33.875712;
			      lng=151.209204;
			    } 
	


			profile={
				name: name,
          		//car: [{vin:vin}],  //array of cars
          		type: type,		  
          		//genderPreference: null,
          		license: license,
          		campus: campus,
                        campus_lat: lat,
			campus_lng:lng,
				regNumber:regNumber,
				brand:brand,
				model:model,
				isBothTypes: isBothTypes,
				capacity:capacity,
				terms: terms  
		  	//	goOnline: goOnline,
		  	//	currLocation: currLocation,
		  	//	currLocation_str: currLocation_str,
		  	//	sendRequest: sendRequest,
		  	//	pickUp: pickUp
		 	};	
		
			Accounts.createUser({
    			email: email,
    			password: password,
				profile:profile
			}, function(error){
					if(error){
						if(error.reason == "Email already exists."){
            				validator.showErrors({
            					email: "That email already belongs to a registered user."   
        					});
            			}
    				} 
    				else
    				{
        				Router.go('welcome'); // Redirect user if registration succeeds
        				//Modal.hide('register');
        			}
        		});
	    	} 
		});
	});


	$.validator.setDefaults({
    	rules: {
	        email: {
	            required: true,
	            email: true
	        },

	         cEmail: {
	            required: true,
	            equalTo: "#email",
	           // email: true
	        },
	        password: {
	            required: true,
	            minlength: 6
	        },
	        termsOfUse: {
	        	 required: true
	        }
    	},
	    messages: {
	        email: {
	            required: "You must enter an email address.",
	            email: "You've entered an invalid email address."
	        },
	        password: {
	            required: "You must enter a password.",
	            minlength: "Your password must be at least {0} characters."
	        },
	        cEmail:{
	        	required: "You must confirm the email.",
	        	equalTo: "confirm email should be same as email address entered."
	        },
	        termsOfUse:{
	        	required: "Please accept the terms of use"	
	        }
	    }
	});


