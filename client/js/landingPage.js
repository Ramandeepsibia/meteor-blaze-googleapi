/**
* We are not using this yet
* It shows a landing page to users prior to the login register page
**/

Template.landingPage.events({
  	'click #login': function(e) {
    e.preventDefault();

    Modal.show('login');
  }
  
});