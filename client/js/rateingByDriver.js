/** 
* This js file contains the helpers and events 
* rateingByDriver.html template to assist drivers rate the riders
**/

Template.rateingByDriver.helpers({
	

	driverDetails: function()
	{

		if(Session.get('driverDetailsDest')){
			return Session.get('driverDetailsDest');
		}
	},
	from: function()
	{
		var tripId = Session.get("tripID");
		var tripHistoryObject= tripHistory.findOne({
				_id: tripId
			});
		var obj = tripHistoryObject;
		//console.log("what is it...."+obj.riderPickUpAddress);
		return obj.riderPickUpAddress;
	},
	toAddr: function()
	{
		var tripId2 = Session.get("tripID");
		var tripHistoryObject2= tripHistory.findOne({
				_id: tripId2
			});
		var obj2 = tripHistoryObject2;
		//console.log("what is it...."+obj.riderPickUpAddress);
		return obj2.riderDestinationAddress;
	},
	travel_dist: function()
	{
		 var travel_dist = Session.get(travel_dist);
		 return travel_dist;
	},
	travel_time: function()
	{
		
		var travel_time=  Session.get(travel_time);
		return travel_time;
	}


});


Template.rateingByDriver.events({  
	'click .js-rate-image':function(event){
			console.log("you clicked a star");
			var rating = $(event.currentTarget).data("userrating");
			console.log(rating);
			//var userid = this.id;
			//console.log(userid);

			var tripId = Session.get("tripID");
			
			tripHistory.update({_id: tripId}, {$set: {"tripRatingRiderGot": rating}});
			

	}

});