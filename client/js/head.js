/** 
* This js file contains the helpers and events for to assist 
* head.html template 
* Assists users logging out and makes menu icons show different screens
**/

Template.head.helpers({
  /**
  * Retuns the name of the user
  **/
  username:function(){
    if(Meteor.user()){
      return Meteor.user().profile.name;
      //return Meteor.user().profile.car[0].vin;

      //return Meteor.user().emails[0].address;
    }
    else{
      return "anonymous internet user??";
    }
  },

  userType:function(){
     if(Meteor.user()){
      //return Meteor.user().profile.type;
       if(Meteor.user().profile.type == "driver"){
          return true;
       }
       else
       {
        return false;
       }
      //return Meteor.user().profile.car[0].vin;

      //return Meteor.user().emails[0].address;
    }

  },

  isBoth:function(){
     if(Meteor.user()){
      //return Meteor.user().profile.type;
       if(Meteor.user().profile.isBothTypes == true){
          return true;
       }
       else
       {
        return false;
       }
      //return Meteor.user().profile.car[0].vin;

      //return Meteor.user().emails[0].address;
    }

  }
});


Template.head.events({
  /**
  *Menu icons clicks and a different modal is shown each time
  **/
  'click #home': function(e) {
    e.preventDefault();
    Router.go('welcome');
  },

  'click #vehicles': function(e) {
    e.preventDefault();
    Modal.show('helpModal');
  },

  

  /**
  * logging out users and 
  * if the user is a driver make his unavailable to take anymore requests
  **/

  'click #logout': function(event){
    event.preventDefault();
    console.log("logged out-->");   
    var user_obj = Meteor.users.findOne({_id:Meteor.userId()});
    console.log("user obj: "+user_obj);
    var location_object = userLocations.findOne({userId:Meteor.userId()});
    console.log("location_object: "+location_object);
    if(user_obj.profile){
      if((user_obj.profile.type==="driver")&&(location_object!=null)&&(location_object.OnlineStatus===true))
      {
         console.log("logged out: "+user_obj.profile.type);
         userLocations.update({_id: location_object._id}, {$set: {
          "OnlineStatus":false
          }
        });                
      }
    }
    

    Session.clear();
    Meteor.logout();
    Router.go('/');
  
  }    
});
  