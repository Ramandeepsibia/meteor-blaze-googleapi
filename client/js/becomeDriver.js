/** 
* This js file contains the events for 
* becomeDriver.html template 
* This file is used when a rider wants to drive
**/

Template.becomeDriver.events({
  'click #becomeDriver'(event) {
      // Prevent default browser form submit
      
    event.preventDefault();


    swal("Congrats!", "You can start driving now!.", "success",);
    //Getting form fields
    var license =$('[name=license]').val();
    var regNumber=$('[name=regNumber]').val();
    var gender = $('[name=gender]').val();
    var model=$('[name="model"]').val();
    var brand=$('[name="brand"]').val();
    var capacity=$('[name="capacity"]').val();
    var type = "driver";
    var isBothTypes = true;


    //Updating the dataase
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.license": license}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.regNumber": regNumber}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.genderPreference": gender}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.brand": brand}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.model": model}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.capacity": capacity}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.type": type}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.isBothTypes": isBothTypes}});

    // Redirect user if registration succeeds

    Router.go('welcome'); 
  },


  'click #cancel'(event) {
    // Prevent default browser form submit
    //var conf = true;

    event.preventDefault();

    Router.go('welcome'); // Redirect user if registration succeeds
  }

});