/** 
* This js file contains the helpers and events for to assist 
* driverToPickUp.html template 
* when driver has accepted the trip request
**/
Template.driverToPickUp.helpers({
    /**
    * Returns the first name of the rider 
    **/   

    riderName: function(){  
       var user = Meteor.users.findOne(Session.get("riderid"));
         var firstName = user && user.profile && user.profile.name;
         console.log(firstName);
         return firstName;
    },

    /**
    * Returns the status of driver whether reached at pick up address or not yet 
    **/
    reachedPickUp: function(){
        return Session.get("reachedAtPickup");
    },

    /**
    * Returns the url for map with the starting point being the 
    * drivers current address and the destination being the 
    * riders pick up destination address. 
    **/
    pathGoogleMaps: function(){
        /**
        *Getting the latitude and longitude  for the map origin and destination
        *from the session
        **/
        var tripID = Session.get("tripID");

        console.log("Ma trip id is ------" + tripID);
        var tripObj=Trips.findOne({_id:tripID}); 
        var tripObj=tripObj;


        //Getting destination coords of rider   
        userLocationsObj_rider = userLocations.findOne({userId:tripObj.rider_id});
        var lat_rider_dest = userLocationsObj_rider.location_destination.lat;
        var lng_rider_dest = userLocationsObj_rider.location_destination.lng;


        //Getting source coords of rider   
       // userLocationsObj_rider = userLocations.findOne({userId:tripObj.rider_id});
        var lat_rider = userLocationsObj_rider.location.lat;
        var lng_rider = userLocationsObj_rider.location.lng;

        /**
        *Getting the location address of rider
        **/
          
        reverseGeocode.getLocation(lat_rider, lng_rider, function(l){
    
           // Session.set('location', reverseGeocode.getAddrStr());
            var riderSourceAddress = reverseGeocode.getAddrStr();
            
            console.log("riderSourceAddress" + riderSourceAddress);
            Session.set('riderSourceAddress', riderSourceAddress);
        });

        //Getting source coords of driver 
        userLocationsObj_driver = userLocations.findOne({userId:tripObj.driver_id});
        var lat_driver = userLocationsObj_driver.location.lat;
        var lng_driver = userLocationsObj_driver.location.lng;

        /**
        *Getting the location address of driver
        **/
        reverseGeocode.getLocation(lat_driver, lng_driver, function(l){
    
            // Session.set('location', reverseGeocode.getAddrStr());
            var driverSourceAddress = reverseGeocode.getAddrStr();
            console.log("driverSourceAddress" + driverSourceAddress);
            Session.set('driverSourceAddress', driverSourceAddress);

        });

        console.log(lng_driver);

        //Setting the coords of drievrs's address in session

        Session.set('lat_driver', lat_driver);
        Session.set('lng_driver', lng_driver);

        //Creating an origin point for the map
        var origin ;
        if (Session.get('lat_driver') != null && Session.get('lat_driver') != null) {
            origin= Session.get('lat_driver') + "," +Session.get('lng_driver');
        }

        console.log(origin);

        var path = "";
        if ( origin != undefined && origin != null ) {
            //Creating a destination point for the map
            var destination = lat_rider + "," + lng_rider;
            console.log(origin);
            path = "https://www.google.com/maps/embed/v1/directions"
                + "?key=AIzaSyAXFzehVMiKB2yNIeI467aeoBUBrbg1H5Q";
            path+= "&origin=" + origin;
            path+=  "&destination="+ destination;
            //path+= "&waypoints="+lat_rider_dest+","+lat_rider_dest;
            path+=  "&avoid=tolls|highways";
            console.log(path);
        } else {
            // When I don't have the origen and destination
            path = "https://www.google.com/maps/embed/v1/place"
                + "?key=AIzaSyAXFzehVMiKB2yNIeI467aeoBUBrbg1H5Q";
            path+= "&q=" + this.street + "," + this.addressNumber + "," + this.neighborhood + "," + this.city ;
            path+= "&zoom=14&maptype=roadmap";
            console.log(path);
        }
        return path;
    }
});



 Template.driverToPickUp.events({
    /**
    * An event to fire when a driver reaches at the riders pick up location
    **/

   'click #atPickUpLoc':function(event){

        //Getting trip id from session variable

        var tripID = Session.get("tripID");
        
        // update the trips collection
        Trips.update(
            {"_id":tripID},
            {$set: 
                {"reachedAtPickup": true}
            }
        );   
      
        Session.set("reachedAtPickup", "true");  
    },   

    'click #startTripBTn':function(event){

        // update the databse

        //set a reactive var
      
        Session.set("startTrip", "true");  

        var tripID = Session.get("tripID");
        //  var tripObj=Trips.findOne({_id:tripID}); 
        Trips.update(
            {"_id":tripID},
                {$set: 
                    {"startedTrip": true}
                }
        ); 

        // redirect to another screen with new map (navigate to destination)
        Router.go('/driveToFinalDestination');

    },    
    /**
    * Event to fire when driver needs turn by turn directions 
    * to drive to the riders destination address from his current location
    **/
    'click #me': function () {
       // Get riders pickUp and Destinarion address
       var driverSourceAddress = Session.get("driverSourceAddress");
       var riderSourceAddress  = Session.get("riderSourceAddress");

       // Launch external navigator with prefilled from and to fields
       plugin.google.maps.external.launchNavigation({     
        "from": driverSourceAddress,
        "to": riderSourceAddress
       });
     
    }
});