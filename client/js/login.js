/** 
* This js file contains the helpers and events for to assist 
* login.html template 
* Assists users login to the app
**/
Template.login.onCreated(function(){
    	console.log("The 'login' template was just created.");

   
	});


	/*
	this code logs in a user. If login is successful then a welcome template is rendered otherwise errors are displayed on the login page
	*/
	Template.login.onRendered(function(){
    	var validator = $('.login').validate({
			submitHandler: function(event){
            	console.log("You just submitted the 'login' form.");
				var email = $('[name=email]').val();
        		var password = $('[name=password]').val();		
				
				Meteor.loginWithPassword(email, password, function(error){
					if(error){
						if(error.reason == "User not found"){
							validator.showErrors({
								email: "That email doesn't belong to a registered user."      
	    					});
						}
						if(error.reason == "Incorrect password"){
							validator.showErrors({
								password:  "You entered an incorrect password."
							});
						}
					}
			 		else{
			 			console.log("userid is: "+Meteor.userId());
			 			var id=Meteor.userId();
			 			var isBothTypes = Meteor.user().profile.isBothTypes;
			 			//Session.setAuth("active", id);
			 			//Session.setPersistent("active",id);
			 			//console.log("active user is: "+Session.get("active"));
			 			if(isBothTypes == true){
			 			
			 			Router.go('isBoth');}
			 			else
			 			{
			 				Router.go('welcome');
			 			}
			 			//Modal.hide('login');

			 		}			
				});
       
        	}	
		});
		
	});

	Template.login.onDestroyed(function(){
    	console.log("The 'login' template was just destroyed.");
	});


	Template.login.events({
    	'submit form': function(event){
        	event.preventDefault();
        	//Modal.hide('login');
      	},


      	'click #me': function(e)
      	{
      		e.preventDefault();
      	},

	   'click #signUpModaldh': function(e) {
		    e.preventDefault();

		    Router.go('/register');
       		
		}

    });

