/** 
* This js file contains the helpers and events for to assist 
* driver_online.html template 
* Assists drivers become available and view/accept/ reject the trip requests
**/

var lat;
var lng;
var source_loc;

var des_lat;
var des_lng;

var TC_lat=-37.72076119999999;
var TC_lng=145.04709360000004;

var latrobe_lat= -37.72;
var latrobe_lng= 145.05;

var distance;
var isOnline = false;
//userLocation document object
var obj; 

var driverStatus;

var locationSor = new ReactiveVar();
var geo;

Template.driver_online.onRendered(function () {
	this.autorun(() => {

	  	/**
	  	*Fetch the current loc of user and prefill
	  	***/

	  	var latLng1 = Geolocation.latLng();
	  	
	    if( latLng1 !=  null)
	    {
     		var lati = latLng1.lat;
     		var longi = latLng1.lng;
     		reverseGeocode.getLocation(lati, longi, function(l){

			    // Session.set('location', reverseGeocode.getAddrStr());
			    var sourceAddress = reverseGeocode.getAddrStr();
			    locationSor.set(reverseGeocode.getAddrStr());

			    /**
			    * Calculate the distance between drivers current location coordinates
			    * and la trobe university to find out whether the driver is at uni 
			    * or going to uni
			    **/
			    distance=geolib.getDistance( 		
		 		    {latitude: lati, longitude: longi},
		 	       	{latitude: TC_lat, longitude: TC_lng}
				    );
				distance=distance/1000;

				obj = userLocations.findOne({userId:Meteor.userId()});

				// Case : when whe driver is far away from uni
				if(distance>1)
				{   	
					var user = Meteor.users.findOne({_id:Meteor.userId()});
                    var address=user.profile.campus;
					
					//geo = new google.maps.Geocoder();
					
  
				  // 	$('[name=destination]').val("La Trobe University Bundoora");  //set location in text field
				   	$('[name=destination]').val(user.profile.campus);  //set location in text field
					$('[name=destination]').prop('disabled',true);

					//Enter location address for the first time
				
					var user_lat=user.profile.campus_lat;
					var user_lng=user.profile.campus_lng;
					if(obj==null)
				   	{ 	
				   		obj=userLocations.insert({
				   			"source_name":sourceAddress,
				   			"location":{"lng":longi,"lat":lati},
				   			"destination_name":address,
				   			"location_destination":{"lng":user_lng,"lat":user_lat},
				   			"userId":Meteor.userId(),
							"OnlineStatus":false
						});
					}
					else
					{
						// update the location address
						console.log("update existing loc object");					
					 	userLocations.update({_id: obj._id}, {$set: {
					 		"source_name": sourceAddress,
					 		"location": {"lng":longi,"lat":lati},
					 		"destination_name":address,
					 		"location_destination":{"lng":user_lng,"lat":user_lat} } 
					 	});
					}


					Session.set("locID",obj);  	
				}
				else{

					//Drivers current location is uni insert/ update the location address
			

					if(obj==null)
				   	{ 	
				   		obj=userLocations.insert({
				   			"source_name":sourceAddress, 
				   			"location":{"lng":TC_lng,"lat":TC_lat},
				   			"destination_name":null,
				   			location_destination:{"lng":0,"lat":0},
				   			"userId":Meteor.userId(),
							"OnlineStatus":false
						});
					}
					else
					{
					 	console.log("update existing loc object");					
					 	userLocations.update({_id: obj._id}, {$set: {
					 		"source_name": sourceAddress,
					 		location: {"lng":TC_lng,"lat":TC_lat},
					 		"destination_name":null, 
					 		location_destination:{"lng":0,"lat":0} } 
					 	} );
					}

				}

			});
	    }
	    
    	/**
        *  Reactively update driver's screen when a new trips requests is made.
        *  A request should expire within 30 secs if not accepted
		**/		

		// Get the current system time to count 30 secs.
		var possibleTrips= new Array();
		var currentTime = new Date().getTime();
		var diffTime = currentTime - (15000);
		
		/**
		* Browse the Trips table to find any new trip requests for the driver
		* For each request calculate the distance and time between rider and 
		* drivers current location
		**/
	    Trips.find({
			driver_id: Meteor.userId(),
	    	create_time: { $gt: diffTime}		

		}).forEach(function(j){				
					
			var l = Meteor.users.findOne({_id:j.rider_id});
			//Find the riders location 
			var location_object = userLocations.findOne({userId:j.rider_id});
	   		
	   		//FInd the drivers location
	   		var location_object_driver = userLocations.findOne({userId:Meteor.userId()});
	   		
	   		// Find the distance in km hence divide by 1000
	   		distance=geolib.getDistance( 		
	 			{ latitude: location_object.location.lat, longitude: location_object.location.lng },
	 	       	{ latitude: location_object_driver.location.lat, longitude: location_object_driver.location.lng }
		    );
			distance=distance/1000;
			console.log("distance between rider and driver: "+distance);

			/**
            * Getting the travel time using Google Maps Directions Service
            **/

			var origin = new google.maps.LatLng( location_object.location.lat, location_object.location.lng); // using google.maps.LatLng class
            console.log("origin is:--->>> "+origin);
			var destination = location_object_driver.location.lat + ', ' + location_object_driver.location.lng; // using string
            console.log("destination is----> "+destination);          
           
			var directionsService = new google.maps.DirectionsService();
			var request = {
			    origin: origin, // LatLng|string
			    destination: destination, // LatLng|string
			    travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

            /**
            * calculating the time to travel for the driver
            **/    
            var directionsDisplay;
			directionsService.route( request, function( response, status ) {

			    if ( status === 'OK' ) {
			        var point = response.routes[ 0 ].legs[ 0 ];
			        //console.log("est--> "+point.duration.text+"  and  "+point.distance.text);
			        Session.set("travel_time",point.duration.text);
			        //console.log("INSIDE:");
                }

			} );
			
			/**
			* Push the details of the trip request to an array
			**/
			possibleTrips.push({tripObject_id: j._id,object: l, source_location: location_object.source_name, destination_location: location_object.destination_name, distance:distance, driving_time:Session.get("travel_time")});
	
		});//for each

		/**
		* Setting the array in the session
		**/
        Session.set("riderDetails",possibleTrips);
        stateChange();
        
        /** 
        * set this session for 30 sec and then clear it 
        * so that the driver have the request on their screen for mere 30 sec
        **/ 
          	
        function stateChange() {

			setTimeout(function () {
				
				//console.log("its OUTSIDE THINGY NOW");
	 	 		Session.set("riderDetails","");
	 	 		
				}, 30000);
	   		// return;
		}
    });//autorun
});



Template.driver_online.events({
	/**
	*Get a current location on click on location icon and update in the database
	**/
	'click .getCurrLoc': function(event){
    	event.preventDefault();	

    	var loc=Geolocation.latLng();
		console.log(loc);
		
		lat = loc.lat;
		lng = loc.lng;

		distance=geolib.getDistance( 		
 			{ latitude: lat, longitude: lng },
 	       	{ latitude: TC_lat, longitude: TC_lng }
		);

		distance=distance/1000;
		console.log("distance between TC building and source: "+distance);
					
		reverseGeocode.getLocation(lat, lng, function(location){
		$('[name=source]').val(reverseGeocode.getAddrStr());  //set location in text field
		source_loc=reverseGeocode.getAddrStr();	

		obj = userLocations.findOne({userId:Meteor.userId()});
		console.log("existing loc obj: "+obj);	
	  
		if(distance>1)
		{   	
			var user = Meteor.users.findOne({_id:Meteor.userId()});
            var address=user.profile.campus;
		   	$('[name=destination]').val(address);  //set location in text field
			$('[name=destination]').prop('disabled',true);
			var user_lat=user.profile.campus_lat;
     		var user_lng=user.profile.campus_lng;

		    		    
		    if(obj==null)
		   	{ 	
		   		obj=userLocations.insert({
		   			"source_name":source_loc,
		   			"location":{"lng":lng,"lat":lat},
		   			"destination_name":address,
		   			"location_destination":{"lng":user_lng,"lat":user_lat},
		   			"userId":Meteor.userId(),
					"OnlineStatus":false
				});
			}
			else
			{
				console.log("update existing loc object");					
			 	userLocations.update({_id: obj._id}, {$set: {
			 		"source_name": source_loc,
			 		"location": {"lng":lng,"lat":lat},
			 		"destination_name":address,
			 		"location_destination":{"lng":user_lng,"lat":user_lat} } 
			 	});
			}


			Session.set("locID",obj);
		   	
			console.log("obj---> "+obj);
			console.log("objID---> "+obj._id);		 	
		}//if distance

		else
			{
				if(obj==null)
			   	{ 	
			   		obj=userLocations.insert({
			   			"source_name":source_loc, 
			   			"location":{"lng":TC_lng,"lat":TC_lat},
			   			"destination_name":null,
			   			location_destination:{"lng":0,"lat":0},
			   			"userId":Meteor.userId(),
						"OnlineStatus":false
					});
				}
				else
				{
				 	console.log("update existing loc object");					
				 	userLocations.update({_id: obj._id}, {$set: {
				 		"source_name": source_loc,
				 		location: {"lng":TC_lng,"lat":TC_lat},
				 		"destination_name":null, 
				 		location_destination:{"lng":0,"lat":0} } 
				 	} );
				}
				
				$('[name=destination]').prop('disabled',false);
			   	$('[name=destination]').val("");

			}//else
		 
		});		//reverse geocode
	},
	/**
	* Event to fire when a driver is available
	**/
	'submit form':function(event){
		event.preventDefault();		 
		// update Meteor.user to online
		Session.set("notify", true);

		
		obj = userLocations.findOne({userId:Meteor.userId()});		
		/**if(obj.OnlineStatus == true) 
		{
			swal({   title: "You are already Online. Go Offline?",   
  
				type: "warning",   showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes, Go Offline",   
				closeOnConfirm: false }, 
				function(){   swal("Done!", "You are now offline", "success"); });

				userLocations.update({_id: obj._id},{$set: {OnlineStatus:false}});
				Session.set("already","false");


		}
		else
		{**/
		 Session.keys = {};
		swal("Success!", "You are now Online!", "success")
		userLocations.update({_id: obj._id},{$set: {OnlineStatus:true}});
		Session.set("unavailable","false");

		//}


	 }, 

	 /**'click #becomeUnavailable':function(event){
    event.preventDefault();
	userLocations.update({_id: obj._id},{$set: {OnlineStatus:false}}); 

 	Session.set("already","false");


    }, **/

	 /**
	 *Event to fire when driver rejects a request
	 **/
	'click .js-reject':function(event){
		var trip_id=this.tripObject_id;	
			
		Trips.remove({"_id":trip_id});    
		console.log("and removed");

	},    
	/**
	* Event to fire when a driver accepts the request
	**/
	'click .js-accept':function(event)
	{
		var trip_id = this.tripObject_id;
		Trips.update(
			{"_id":trip_id},
			{$set: 
				{"request_Accepted": true}
			}
		);	   
	    Session.set("tripID",trip_id);



	    /**
		* if a driver accepts the request set the status to offline 
		* to prevent further  getting requests
		**/

		var user_obj = Meteor.users.findOne({_id:Meteor.userId()});
		var location_object = userLocations.findOne({userId:Meteor.userId()});

		if((user_obj.profile.type==="driver")&&(location_object!=null)&&(location_object.OnlineStatus===true))
		{
			console.log("Driver accepted req so status goes offline");
			userLocations.update({_id: location_object._id}, {$set: {"OnlineStatus":false}});                
        }

        // Session.set("tripID",trip_id);
        console.log("Hey I am here");
        Router.go('driverToPickUp');
		
   	}

});//events

Template.driver_online.helpers({
   
	/**
	* Returns the new rider requests to the driver to see
	**/
	userList: function () {					
		
    if(Session.get("riderDetails"))
    	{return Session.get("riderDetails");}

    } ,//userList
    
    /**
    * Returns drivers current location
    **/
    'locationSor': function() {
			console.log(locationSor.get());
		    return locationSor.get();
	},

	'alreadyAvailable': function(){
		return Session.get('already');

	},

	'unavailable': function(){
		return Session.get('unava');

	},
	'notify': function(){
		if(Session.get("notify") == true)
			{
				return Session.get('notify');
			}

	},
});

Template.driver_online.onRendered(function () {

    this.autorun(() => {
	    // Wait for API to be loaded
	    if (GoogleMaps.loaded()) {
	        // Saving locations by Google's Autocomplete only
	  		
			$('#sor').geocomplete().bind("geocode:result",function(event, result){
		        console.log(result);
		        //result.formatted_address
		        lat=result.geometry.location.lat();
		        console.log("latitude: "+lat);
		        lng=result.geometry.location.lng();
		        console.log("longitude: "+lng);                
				
				Session.set("locID",obj);
		   		 
		   		distance=geolib.getDistance( 		
			 		{latitude: lat, longitude: lng},
			 		{latitude: TC_lat, longitude: TC_lng}
				);

		   		distance=distance/1000;
			    console.log("distance between TC building and source in bind method: "+distance);

			    obj = userLocations.findOne({userId:Meteor.userId()});
				console.log("existing loc obj: "+obj);

				if(distance>1)
				{   
				    /*	
				    var user = Meteor.users.findOne({_id:Meteor.userId()});
					console.log("user.campus is@@@@@@@@@: "+user.campus); 
					set des value to preferred campus
					get latitude and logitude also reverse geocode
  					*/
					$('[name=destination]').val("La Trobe University Bundoora");  //set location in text field
					$('[name=destination]').prop('disabled',true);		  
			
	  		 		
				    if(obj==null)
				    { 	
						obj=userLocations.insert({
							"source_name":result.formatted_address, 
							"location":{"lng":lng,"lat":lat},
							"destination_name":"La Trobe University Bundoora",
							location_destination:{"lng":TC_lng,"lat":TC_lat},
							"userId":Meteor.userId(),
							"user":Meteor.user().profile.name,
							"type":Meteor.user().profile.type,
							"requestStatus":Meteor.user().profile.sendRequest,
							"OnlineStatus":Meteor.user().profile.goOnline
						});
					}
					else
					{
						console.log("update existing loc object");
						userLocations.update({_id: obj._id}, {$set: {
						 	"source_name": result.formatted_address,
						 	location: {"lng":lng,"lat":lat},
						 	"destination_name":"La Trobe University Bundoora",
						 	location_destination:{"lng":TC_lng,"lat":TC_lat}
						}});
					}

	        	}//if distance
			
				else
				{
				    //	obj = userLocations.findOne({userId:Meteor.userId()});
				    // console.log("existing loc obj: "+obj);
				    if(obj==null)
				   	{ 	
						obj=userLocations.insert({
							"source_name":result.formatted_address, 
							"location":{"lng":TC_lng,"lat":TC_lat},
							"destination_name":null,
							location_destination:{"lng":0,"lat":0},
							"userId":Meteor.userId(),
							"user":Meteor.user().profile.name,
							"type":Meteor.user().profile.type,
							"requestStatus":Meteor.user().profile.sendRequest,
							"OnlineStatus":Meteor.user().profile.goOnline
						});
					}
					else
					{
						console.log("update existing loc object");
						userLocations.update({_id: obj._id}, {$set: {
							"source_name": result.formatted_address,
							location: {"lng":lng,"lat":lat},
							"destination_name":null,
							location_destination:{"lng":0,"lat":0}
						}});
					}

				   	 $('[name=destination]').prop('disabled',false);
				   	 $('[name=destination]').val("");

				}
	        
	        }); //end of geocomplete.bind

		    $('#dest').geocomplete().bind("geocode:result",function(event, result){
		    	des_lat=result.geometry.location.lat();
	        	console.log("destination lat: "+des_lat);
	        	des_lng=result.geometry.location.lng();  
	        	console.log("destination lng: "+des_lng);    
	        
	        	reverseGeocode.getLocation(des_lat, des_lng, function(location){
					//var	destination_name=reverseGeocode.getAddrStr();	
					var	destination_name= result.formatted_address;			   	
			   		obj = userLocations.findOne({userId:Meteor.userId()});		
		        	
		        	userLocations.update({_id: obj._id}, {$set: {
		        		destination_name:destination_name,
		        		location_destination:{"lng":des_lng,"lat":des_lat}
		        	}});	  
			   	});// reverse geocode

	      	}); //#dest.geocomplete  


	    } //Googlemaps.loaded

    });//autorun

});
