/** 
* This js file contains the helpers and events 
* rateTrip.html template to assist riders rate the drivers
**/
Template.rateTrip.helpers({
	driverDetails: function()
	{

		return Session.get('driverDetailsDest');
	},
	from: function()
	{
		var tripId = Session.get("idOfTrip");
		var tripHistoryObject= tripHistory.findOne({
				_id: tripId
			});
		var obj = tripHistoryObject;
		//console.log("what is it...."+obj.riderPickUpAddress);
		return obj.riderPickUpAddress;
	},
	toAddr: function()
	{
		var tripId2 = Session.get("idOfTrip");
		var tripHistoryObject2= tripHistory.findOne({
				_id: tripId2
			});
		var obj2 = tripHistoryObject2;
		console.log("what is it?..."+obj2.riderDestinationAddress);
		return obj2.riderDestinationAddress;
	}


});

Template.rateTrip.events({  
	'click .js-rate-image':function(event){
			console.log("you clicked a star");
			var rating = $(event.currentTarget).data("userrating");
			console.log(rating);
			//var userid = this.id;
			//console.log(userid);

			var tripId = Session.get("idOfTrip");
			
			tripHistory.update({_id: tripId}, {$set: {"tripRatingDriverGot": rating}});
			

	}

});