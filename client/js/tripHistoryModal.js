/** 
* This js file contains the helpers and events for to assist 
* tripHistoryModal.html template 
* Assists drievrs and riders view their trip history
**/


Template.tripHistoryModal.onRendered(function () {
	/**
	* Browse the Trip Hiostory collection to find all completed trips for the driver
	**/


	//this.autorun(() => {
		if(Meteor.user()){ // if user is logged in 
			
			var allMyTrips= new Array(); var nameOfRider;
			if(Meteor.user().profile.type== "driver")
			{
				
				//var tripId = Session.get("idOfTrip");
				var tripHistoryObject=   tripHistory.find(
					{driverID:Meteor.userId()}, 
					{ sort: { createdAt: -1 }}
				).forEach(function(j){	

					var l = Meteor.users.findOne(
					{
						_id:j.riderID
						
					});

				    nameOfRider = l.profile.name;
					allMyTrips.push({
						tripObject_id: j._id,
						driverID: j.driverID, 
						riderPickUpAddress: j.riderPickUpAddress, 
						riderDestinationAddress: j.riderDestinationAddress, 
						riderID:j.riderID, 
						dateTime:j.dateTime,
						earnings:j.earnings,
						carbonSaved:j.carbonSaved,
						tripRatingDriverGot:j.tripRatingDriverGot,
						tripRatingRiderGot:j.tripRatingRiderGot,
						nameOfRider: nameOfRider
					});
				});//end of foreach
				
				Session.set("tripHistoryDetails",allMyTrips);

				//return tripHistoryObject.riderPickUpAddress;

				//console.log("54645" + tripHistoryObject.riderPickUpAddress);

				//
				
				//return tripHistory.find({driverID:Meteor.userId()}, { sort: {dateTime: -1}});
			}
			else if(Meteor.user().profile.type== "rider")
			{
				
				var tripHistoryObject=   tripHistory.find(
					{riderID:Meteor.userId()}, 
					{ sort: { createdAt: -1 }}
				).forEach(function(j){	

					var l = Meteor.users.findOne(
					{
						_id:j.driverID
						
					});

				    nameOfDriver = l.profile.name;
					allMyTrips.push({
						tripObject_id: j._id,
						driverID: j.driverID, 
						riderPickUpAddress: j.riderPickUpAddress, 
						riderDestinationAddress: j.riderDestinationAddress, 
						riderID:j.riderID, 
						dateTime:j.dateTime,
						earnings:j.earnings,
						carbonSaved:j.carbonSaved,
						tripRatingDriverGot:j.tripRatingDriverGot,
						tripRatingRiderGot:j.tripRatingRiderGot,
						nameOfDriver: nameOfDriver
					});
				});//end of foreach
				
				Session.set("tripHistoryDetails",allMyTrips);
				
			}
			else{
				return tripHistory.find({}, { sort: { createdAt: -1 } });
			}
		}
	////});
});

Template.tripHistoryModal.helpers({
		//returns true if the user is a driver
	userType:function(){
		if(Meteor.user()){
			if(Meteor.user().profile.type== "driver")
			{
				return true;
			}
			else if(Meteor.user().profile.type== "rider")
			{
				return false;
			}
		}
	},
	tripList: function () {					
		
    if(Session.get("tripHistoryDetails"))
    	{return Session.get("tripHistoryDetails");}

    }
});