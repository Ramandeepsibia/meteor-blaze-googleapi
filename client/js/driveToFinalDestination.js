/** 
* This js file contains the helpers and events for to assist 
* driveToFinalDestination.html template 
* when driver has started the trip
**/

var riderId;
var earnings;
var carbonSaved; 


Template.driveToFinalDestination.helpers({
    /**
    * Returns the url for map with the starting point being the 
    * riders pick-up address and the destination being the 
    * riders final destination address. 
    **/
   
   	pathGoogleMapsDest: function(){
        //Get the trip id from session

   	    var tripID = Session.get("tripID");

        //console.log(tripID);
        var tripObj=Trips.findOne({_id:tripID}); 

        riderId = tripObj.rider_id;

        //Getting source coords of rider's pickup location from trips collections
        userLocationsObj_rider = userLocations.findOne({userId:tripObj.rider_id});
        var lat_rider = userLocationsObj_rider.location.lat;
        var lng_rider = userLocationsObj_rider.location.lng;

        /**
        *Get the location address of rider
        **/
          
        reverseGeocode.getLocation(lat_rider, lng_rider, function(l){
            // Session.set('location', reverseGeocode.getAddrStr());
            var riderSourceAddress = reverseGeocode.getAddrStr();
            
            console.log("riderSourceAddress" + riderSourceAddress);
            Session.set('riderSourceAddress', riderSourceAddress);

        });

        //Getting source coords of riders destination location 
        userLocationsObj_riderDest = userLocations.findOne({userId:tripObj.rider_id});
        var lat_riderDest = userLocationsObj_riderDest.location_destination.lat;
        var lng_riderDest = userLocationsObj_riderDest.location_destination.lng;

        /**
        *Getting the location address of rider destination
        **/

        reverseGeocode.getLocation(lat_riderDest, lng_riderDest, function(l){
    
            // Session.set('location', reverseGeocode.getAddrStr());
            var riderDestinationAddress = reverseGeocode.getAddrStr();
            console.log("rider destination address" + riderDestinationAddress);
            Session.set('riderDestinationAddress', riderDestinationAddress);

        });



        /***
        *Calculating the distance
        * & time between pick up points and the final destination
        ***/
        // getting the distance among them in kms
        var distance=geolib.getDistance(    
          { latitude: lat_rider, longitude: lng_rider },
              { latitude:lat_riderDest, longitude: lng_riderDest }
          );
        distance=distance/1000;
        console.log("distance in kms between rider and driver: "+distance);

        /**
        * Using the directions service to find out the time of travel between two
        **/

        var originPoints = lat_rider+ ', ' + lng_rider; // using google.maps.LatLng class
              
        var destinationPoints = lat_riderDest + ', ' + lng_riderDest; // using string
              

        var directionsService = new google.maps.DirectionsService();
        var request = {
            origin: originPoints, // LatLng|string
            destination: destinationPoints, // LatLng|string
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        /**
              * calculating the time to travel for the driver
              **/    
        var directionsDisplay;
        directionsService.route( request, function( response, status ) {

            if ( status === 'OK' ) {
                var point = response.routes[ 0 ].legs[ 0 ];
                console.log("est--> "+point.duration.text+"  and  "+point.distance.text);
                Session.set("travel_time",point.duration.text);
                Session.set("travel_dist",point.distance.text);
                  }

        });


        //ends calculation of pickup points

        
        //Setting the source coords of rider's pickup location in session
        Session.set('lat_rider', lat_rider);
        Session.set('lng_rider', lng_rider);

        //Creating an origin point for the map
        var origin ;
        if (Session.get('lat_rider') != null && Session.get('lat_rider') != null) {
            origin= Session.get('lat_rider') + "," +Session.get('lng_rider');
        }

        //console.log(origin);

        var path = "";
        if ( origin != undefined && origin != null ) {
            //Creating a destination point for the map
            var destination = lat_riderDest + "," + lng_riderDest;
            console.log(origin);
            path = "https://www.google.com/maps/embed/v1/directions"
                + "?key=AIzaSyAXFzehVMiKB2yNIeI467aeoBUBrbg1H5Q";
            path+= "&origin=" + origin;
            path+=  "&destination="+ destination;
            path+=  "&avoid=tolls|highways";
            console.log(path);
        } else {
            // When I don't have the origen and destination
            path = "https://www.google.com/maps/embed/v1/place"
                + "?key=AIzaSyAXFzehVMiKB2yNIeI467aeoBUBrbg1H5Q";
            path+= "&q=" + this.street + "," + this.addressNumber + "," + this.neighborhood + "," + this.city ;
            path+= "&zoom=14&maptype=roadmap";
            console.log(path);
        }
        return path;
    }

   });


Template.driveToFinalDestination.events({
    /**
    * Event to fire when a trip is ended
    **/
	'click #endTrip':function(event){

        var tripID = Session.get("tripID");
         
        //Update the trips collection 
        Trips.update(
            {"_id":tripID},
            {$set: 
                {"endedTrip": true}
            }
        ); 

        var riderPickUpAddress = Session.get("riderSourceAddress");
        var riderDestAddress  = Session.get("riderDestinationAddress");
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes();
            console.log(datetime);
        var travel_time = Session.get(travel_time);
        var travel_dist = Session.get(travel_dist);

        //Add the trip details to trip history table
        tripHistory.insert({
            "_id":tripID,
            "driverID":Meteor.userId(),
            "riderPickUpAddress":riderPickUpAddress,
            "riderDestinationAddress": riderDestAddress,
            "riderID":riderId,
            "dateTime":datetime,
            "earnings":"5",
            "carbonSaved": "4",
            "tripRatingDriverGot": "0",
            "tripRatingRiderGot": "0",
            "createdAt": new Date(),
            "travel_time": travel_time,
            "travel_dist": travel_dist
        });
      
        Session.set("endTrip", "true");  
        Session.set("notify", "false");  

        /**
        * Send an alert to the driver after the trip has ended 
        **/

        swal({   title: "Trip has ended",   
            text: "You saved 4 kg carbon emission",   
            timer: 60000,   
            showConfirmButton: true 
        });

        //Redirect to the welcome screen
        Router.go('/rateingByDriver');

    },    
    /**
    * Event to fire when driver needs turn by turn directions 
    * to drive to the riders destination address from riders pickup address
    **/
    'click #meDest': function () {
        // Get riders pickUp and Destinarion address
        var riderPickUpAddress = Session.get("riderSourceAddress");
        var riderDestAddress  = Session.get("riderDestinationAddress");

        // Launch external navigator with prefilled from and to fields
        plugin.google.maps.external.launchNavigation({     
            "from": riderPickUpAddress,
            "to": riderDestAddress
        });
     
    }

});