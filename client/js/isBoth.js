/** 
* This js file contains the events for 
* isBoth.html template 
**/

Template.isBoth.events({
  /**
  *Menu icons clicks and a different modal is shown each time
  **/
  'click #driver': function(e) {
    e.preventDefault();

    var type = "driver";
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.type": type}});
    Router.go('welcome');
  },

    'click #rider': function(e) {
    e.preventDefault();
    var type = "rider";
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.type": type}});

    Router.go('welcome');
  }


})