/** 
* This js file contains the helpers and events for to assist 
* driver_online.html template 
* Assists users change their email and click photos using camera 
* Drivers can change their car details 
* FILE NOT IN USE NOW
**/

Template.settingsModal.helpers({
		/**
    * returns username 
    **/
    username:function(){
		if(Meteor.user()){
			return Meteor.user().profile.name;
		}

		else{
			return "anonymous internet user??";
		}

	},

  /**
  * returns type of user driver or rider
  **/
	usertype:function(){
		if(Meteor.user().profile.type == "driver"){
			return true;
    }
      else{ return false;}
		
		
	},

  /**
  * retuns email of user
  **/
	email:function(){
		if(Meteor.user()){
			return Meteor.user().emails[0].address;
		}
		
	},
  /**
  * retuns photo of user
  **/
	photo: function () {
		return Session.get("photo");
	},

  newVin:function(){
    if(Meteor.user()){
      return Meteor.user().profile.regNumber;
    } 
    
  }

});

/**
*Get images from phone camera or photo library
**/
Session.setDefault('img', null);

  var getPicture = function(opts) {
    MeteorCamera.getPicture(opts, function(err, data) {
      if (err) {
        console.log('error', err);
      }
      if (data) {
        Session.set('img', data)
      }
    });
  };
/**
* Events to edit setiings
**/
Template.settingsModal.events({
  'submit .editme'(event) {
    // Prevent default browser form submit
    //var conf = true;

    event.preventDefault();
    swal("Success!", "Your details have been updated.", "success");
      var updatedName=$('[name=updatedName]').val();
      var updatedEmail=$('[name=updatedEmail]').val();
      var newVin = $('[name=newVin]').val();
      var gender = $('[name=gender]').val();
    

    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.name": updatedName}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"emails.0.address": updatedEmail}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.regNumber": newVin}});
    Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.genderPreference": gender}});


    },

    'click #logout': function(event){
    //event.preventDefault();
    console.log("logged out-->");   
    var user_obj = Meteor.users.findOne({_id:Meteor.userId()});
    console.log("user obj: "+user_obj);
    var location_object = userLocations.findOne({userId:Meteor.userId()});
    console.log("location_object: "+location_object);
    if(user_obj.profile){
      if((user_obj.profile.type==="driver")&&(location_object!=null)&&(location_object.OnlineStatus===true))
      {
         console.log("logged out: "+user_obj.profile.type);
         userLocations.update({_id: location_object._id}, {$set: {
          "OnlineStatus":false
          }
        });                
      }
    }
    

    Session.clear();
    Meteor.logout(); 

    Router.go('/');
    
    Modal.hide('settingsModal');

   
  
  }    



});
/**
* Camera and photo library events
**/
Template.cameraEvent.events({
  'click button': function () {
    getPicture({
      width: 350,
      height: 100,
      quality: 75
    });
  }
});

Template.libraryEvent.events({
  'click button': function () {
    if (Meteor.isCordova) {
      getPicture({
        width: 350,
        height: 100,
        quality: 75,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
      });
    } else {
      alert('Cordova only feature.');
    }
  }
});

Template.img.helpers({
  img: function() {
    return Session.get('img');
  }
});