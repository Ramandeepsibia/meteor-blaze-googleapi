/** 
* This js file contains the helpers and events for to assist 
* hellorider.html template 
* Assists rider sending trip requests
**/

var lng1;
var lat1;
var loc2;

var des_lat;
var des_lng;

var TC_lat=-37.72076119999999;
var TC_lng=145.04709360000004;

var latrobe_lat= -37.72;
var latrobe_lng= 145.05;

var distance;

var obj; //userLocation document object

var locationSor = new ReactiveVar();
var myLng = new ReactiveVar();
var myLat = new ReactiveVar();
var lati; var longi;

var pickupAddress;
var destinationAddress;




/**
* Getting the current location of rider at template render and 
* prefilling in the from input field
**/
Template.hellorider.onRendered(function(){

	
   //if (Meteor.isCordova) {

     //	console.log("I m cordova");


  /**	document.addEventListener("deviceready", function () 
  		{ console.log('DEVICE READY FIRE'); }, false);


 document.addEventListener('push-notification', function(event){
      if (device.platform === "iOS") {
        //get the notification payload
        var notification = event.notification;

        //display alert to the user for example
        alert(notification.aps.alert);
      } else if (device.platform === "Android") {

      	
        var title = event.notification.title;
        var userData = event.notification.userdata;
        console.log("I am cught notification");

        if(typeof(userData) != "undefined") {
          console.warn('user data: ' + JSON.stringify(userData));
        }

        alert(title);
      }
    }); **/
// }

    	
 	
  	var latLng1 = Geolocation.latLng();
  	
    if( latLng1 !=  null)
    {
 		lati = latLng1.lat;
 		longi = latLng1.lng;


 		myLng.set(latLng1.lat);
 		myLat.set(latLng1.lng);

 		/**
 		* Set lat lng in session for next template
 		**/
 		Session.set("riderlat",latLng1.lat);  	
 		Session.set("riderlng",latLng1.lng);  	

 		/**
 		* Getting the location address from latitudes and longitudes
 		**/
     	reverseGeocode.getLocation(lati, longi, function(l){

		    // Session.set('location', reverseGeocode.getAddrStr());
		   	var sourceAddress = reverseGeocode.getAddrStr();
		    locationSor.set(reverseGeocode.getAddrStr());

		    /**
		    * calculating how far the rider is from la trobe university
		    **/
		    distance=geolib.getDistance( 		
	 		    {latitude: lati, longitude: longi},
	 	       	{latitude: TC_lat, longitude: TC_lng}
			    );

			distance=distance/1000;

			obj = userLocations.findOne({userId:Meteor.userId()});

			/**
			* Prefilling the to field if driver is quite far from uni
			**/
			if(distance>1)
			{   	
				var user = Meteor.users.findOne({_id:Meteor.userId()});
                var address=user.profile.campus;
			   	$('[name=destination]').val(address);  //set location in text field
			   	$(".estimate").show();
				$('[name=destination]').prop('disabled',true);
				var user_lat=user.profile.campus_lat;
				var user_lng=user.profile.campus_lng;


				if(obj==null)
				{ 	
					userLocations.insert({
						"source_name":sourceAddress,
						"location":{"lng":longi,"lat":lati},
						"destination_name": address,
						"location_destination":{"lng":user_lng,"lat":user_lat},
						"userId":Meteor.userId()
						});
				}	
				else
				{
					console.log("update existing loc object");
					userLocations.update({ _id: obj._id}, {$set: {
						source_name: sourceAddress,
						location: {"lng":longi,"lat":lati},
						"destination_name":address,
				 		"location_destination":{"lng":user_lng,"lat":user_lat} } 
						});
						 
				}

				Session.set("locID2",obj);  	
			}
			else{

				if(obj==null)
				{ 	
					userLocations.insert({
					    "source_name":sourceAddress,
					    "location":{"lng":longi,"lat":lati},
					    "destination_name": null,
					    "location_destination":{"lng":0,"lat":0},
						"userId":Meteor.userId()
					});
				}
				else
				{
					console.log("update existing loc object");
					userLocations.update({ _id: obj._id}, {$set: {
						source_name: sourceAddress,
						location: {"lng":longi,"lat":lati},
						"destination_name":null,
				 		"location_destination":{"lng":0,"lat":0} } 
					});
						 
				}

			}



		});
    }

    /**
    * Rider gets the name of the driver after accepting the request
    * / vehicle registration number/ time of arrival 
    * and distance in the real time.
    
    var driverName;
    var driverDetails= new Array();
    this.autorun(function(){
    	userLocations.find({
			location: 
			{ $near:[lng1, lat1],
			  $maxDistance:0.10						
		    },
		    request_Accepted: true

		}).forEach(function(j){
			 var l = Meteor.users.findOne(
			 {
				_id:j.userId, 'profile.type': "driver"
				
			 });
			// Getting the name of the driver
			driverName = l.profile.name;
			console.log("My drivers name is  " + driverName);
			
			//Getting the location co-ordinates of the driver 
			var driverLocCords = userLocations.findOne({userId:j.userId});

			//Getting riders location co-ordinates
	   		var riderLocCords = userLocations.findOne({userId:Meteor.userId()});

			// getting the distance among them in kms
	   		distance=geolib.getDistance( 		
	 			{ latitude: riderLocCords.location.lat, longitude: riderLocCords.location.lng },
	 	       	{ latitude: driverLocCords.location.lat, longitude: driverLocCords.location.lng }
		    );
			distance=distance/1000;
			console.log("distance in kms between rider and driver: "+distance);

			/**
			* Using the directions service to find out the time of travel between two
			**

			var origin = driverLocCords.location.lat+ ', ' + driverLocCords.location.lng; // using google.maps.LatLng class
            
			var destination = riderLocCords.location.lat + ', ' + riderLocCords.location.lng; // using string
            

			var directionsService = new google.maps.DirectionsService();
			var request = {
			    origin: origin, // LatLng|string
			    destination: destination, // LatLng|string
			    travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

			**
            * calculating the time to travel for the driver
            **  
            var directionsDisplay;
			directionsService.route( request, function( response, status ) {

			    if ( status === 'OK' ) {
			        var point = response.routes[ 0 ].legs[ 0 ];
			        console.log("est--> "+point.duration.text+"  and  "+point.distance.text);
			        Session.set("travel_time",point.duration.text);
			        
                }

			} );


			driverDetails.push({
				tripObject_id: j._id,
				object: l, 
				source_location: location_object.source_name, 
				destination_location: location_object.destination_name, 
				distance:distance, 
				driving_time:Session.get("travel_time")
			});
	
		});

	});**/


    
});

Template.hellorider.helpers({
	/**
	* Notify the rider as a driver accpets the request
	**/
	'notify': function(){
		if(Session.get("RequestSubmitted") == true){
			return Session.get('RequestSubmitted');
		}

	},
	/**
	* Returns current location of rider
	**/
	'locationSor': function() {
		console.log(locationSor.get());
	    return locationSor.get();
	}, 

	/**
	* Healper method to count the no of latrober drivers near by 
	
	'noOfDriversNearMe': function(){

		var log1;
		var possibleTrips= new Array();

	   
        if(myLng.get()){
        	console.log(myLng.get()  +" ////" + myLat.get());
        	var lng = myLng.get();
        	var lt = myLat.get();

			userLocations.find({
				location: 
				{ $near:[lng, lt],
				  $maxDistance:0.10						
			    },
			    OnlineStatus: true

			}).forEach(function(j){
				var l = Meteor.users.findOne(
				{
					_id:j.userId
					
				});

				log1 = l.profile.name;
				alert( log1 );
				console.log("hi hi " + log1);
 				possibleTrips.push({riderId: Meteor.userId() , driverId:l._id});

			});


			var noOfPossibleTrips = possibleTrips.length;

			console.log("Drivers near me" + noOfPossibleTrips);

			if(noOfPossibleTrips ==0){
				return null;
			}
			else{
				return "1";
			}
		}
		else{

			return "1";
		}
	}**/
	
});


Template.hellorider.events({  
	/**
	* Getting current location on click of location icon in the from field
	**/
	'click .getCurrLoc': function(event){
	    event.preventDefault();		

		var loc=Geolocation.latLng();
		console.log("a try" +loc);
		//console.log("got current loc");
		//Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.currLocation": loc}});
		 			   
		lat1 = loc.lat;

		lng1 = loc.lng;

		/**
 		* Set lat lng in session for next template
 		**/
 		Session.set("riderlat",loc.lat);  	
 		Session.set("riderlng", loc.lng);  	

		distance=geolib.getDistance( 		
 		    {latitude: lat1, longitude: lng1},
 	       	{latitude: TC_lat, longitude: TC_lng}
		    );
		distance=distance/1000;
	
		reverseGeocode.getLocation(lat1, lng1, function(location){
			$('[name=source1]').val(reverseGeocode.getAddrStr());  //set location in text field
			loc2=reverseGeocode.getAddrStr();
		
			obj = userLocations.findOne({userId:Meteor.userId()});

			if(distance>1)
			{   	
			   	$('[name=destination]').val("La Trobe University Bundoora");  //set location in text field
				$('[name=destination]').prop('disabled',true);
				$(".estimate").show();
			   	
				//console.log("existing loc obj: "+obj);

				if(obj==null)
				{ 	
					userLocations.insert({
						"source_name":loc2,
						"location":{"lng":lng1,"lat":lat1},
						"destination_name": "La Trobe University Bundoora",
						"location_destination":{"lng":TC_lng,"lat":TC_lat},
						"userId":Meteor.userId()
					});
				}	
				else
				{
					console.log("update existing loc object");
					userLocations.update({ _id: obj._id}, {$set: {
						source_name: loc2,
						location: {"lng":lng1,"lat":lat1},
						"destination_name":"La Trobe University Bundoora",
				 		"location_destination":{"lng":TC_lng,"lat":TC_lat} } 
						});
						 
				}

				Session.set("locID2",obj);  		
			}
			else
			{
			   	if(obj==null)
				{ 	
					userLocations.insert({
					    "source_name":loc2,
					    "location":{"lng":lng1,"lat":lat1},
					    "destination_name": null,
					    "location_destination":{"lng":0,"lat":0},
						"userId":Meteor.userId()
					});
				}
				else
				{
					console.log("update existing loc object");
					userLocations.update({ _id: obj._id}, {$set: {
						source_name: loc2,
						location: {"lng":lng1,"lat":lat1},
						"destination_name":null,
				 		"location_destination":{"lng":0,"lat":0} } 
					});
						 
				}

				$('[name=destination]').prop('disabled',false);
				$('[name=destination]').val("");
				$(".estimate").hide();

			}
		});
	},
	/**
	* Request the system to send requests to the near by drivers
	* until one of them accepts it
	**/
	'submit form':function(event){
		event.preventDefault();	

		//Meteor.call("pushMe");
		/**
		* Send requests to the near by drivers and 
		* notify rider if no driver near to me
		***/

		

		var driverName;
		var carRego;
		var possibleTrips= new Array();

		if (Meteor.user()){
		// find near by drievrs
		userLocations.find({
			location: 
			{ $near:[lng1, lat1],
			  $maxDistance:0.10						
		    },
		    OnlineStatus: true

		}).forEach(function(j){
			 var l = Meteor.users.findOne(
			 {
				_id:j.userId, 
				'profile.type': "driver"
				
			 });
			 var l =l;
			driverName = l.profile.name;
			console.log("66786786767867"+driverName);
			carRego = l.profile.regNumber;
			possibleTrips.push({
				riderId: Meteor.userId(), 
				driverId:l._id,
				driverName: driverName,
				carRego: carRego
			});

		});


		var noOfPossibleTrips = possibleTrips.length;

		console.log(noOfPossibleTrips);

		if(noOfPossibleTrips ==0){

			swal({   title: "Sorry, There is no driver available at your location",   
			text: "Please try again later..",   
			timer: 60000,   
			confirmButtonColor: "#7EC0EE",
			showConfirmButton: true });
			//$(".estimate").hide(); 

		}
		else{

			swal({   title: "Requesting...",   
			text: "A Latrober Driver near to you",   
			timer: 60000,   
			confirmButtonColor: "#7EC0EE",
			showConfirmButton: true });

			console.log(noOfPossibleTrips);

			Session.set("RequestSubmitted",true);

			var breakForLoop = false;

			for (var i = 0; i < noOfPossibleTrips; i++) {
			    
			if( i >= noOfPossibleTrips){

				console.log("loop exited");
				Session.set("RequestSubmitted","false");

			}

			if(breakForLoop == true)
			{
				break;
			}
			else{
				stateChange(i);
			}

		}


	}// for loop ends here
	/**
	* System waits  for 30 secs otherwise 
	* send the request to the next near by driver
	**/
	function stateChange(i) {

		setTimeout(function () {

			obj=Trips.insert(
				{
					"driver_id":possibleTrips[i].driverId, 
					"rider_id":possibleTrips[i].riderId,
					"request_Sent":true, 
					"create_time": new Date().getTime(), 
					"request_Accepted": false
				});
			

			//Check req status  if changes to true within 30 sec
			console.log(obj+  "id is");

			Session.set("tripID",obj);


			Trips.find({_id :obj}).observe({

			    changed:function(new_document, old_document){
			    

					breakForLoop = true;
						

				    Session.set('req_Accpted', "true");

		        	swal({   title: "hey! Your Driver is on the way",   
					text: "Please be at your pickup address",   
					timer: 60000,   
					showConfirmButton: true 
					});


					
					Session.set("driver_id",possibleTrips[i].driverId);
					Session.set("driverName",possibleTrips[i].driverName);
					Session.set("carRego",possibleTrips[i].carRego);

					Router.go('myTemplate');

			    }
			});

			console.log("hii..." + i);

	    }, i * 15000);
	   // return;
	}
}//if user
   
}//if submit

});

Template.hellorider.onRendered(function () {
  
    // Wait for API to be loaded

    this.autorun(() => {

      	if (GoogleMaps.loaded()) {

	  		/**
		    * Get the current location at template render and
		    * update it in the input field
		    **/

			// alert("I am working");

			$('#sor').geocomplete().bind("geocode:result",function(event, result){
				lat1=result.geometry.location.lat();
		       // console.log("latitude: "+lat);
		        lng1=result.geometry.location.lng();
		        //console.log("longitude: "+lng);                
				
				//Session.set("locID",obj);

				/**
		 		* Set lat lng in session for next template
		 		**/
		 		Session.set("riderlat",lat1);  	
		 		Session.set("riderlng", lng1);  	


				distance=geolib.getDistance( 		
		 		    {latitude: lat1, longitude: lng1},
		 	       	{latitude: TC_lat, longitude: TC_lng}
				    );
				distance=distance/1000;
		   		 
				obj = userLocations.findOne({userId:Meteor.userId()});

				if(distance>1)
				{   	
				
					$('[name=destination]').val("La Trobe University Bundoora");  //set location in text field
					$('[name=destination]').prop('disabled',true);	
					$(".estimate").show();

					if(obj==null)
					{ 	
						userLocations.insert({
							"source_name":result.formatted_address,
							"location":{"lng":lng1,"lat":lat1},
							"destination_name": "La Trobe University Bundoora",
							"location_destination":{"lng":TC_lng,"lat":TC_lat},
							"userId":Meteor.userId()
							});
					}	
					else
					{
						console.log("update existing loc object");
						userLocations.update({ _id: obj._id}, {$set: {
							source_name: result.formatted_address,
							location: {"lng":lng1,"lat":lat1},
							"destination_name":"La Trobe University Bundoora",
					 		"location_destination":{"lng":TC_lng,"lat":TC_lat} } 
							});
							 
					}

					Session.set("locID2",obj);  	

				}// if ends here
				else{

					if(obj==null)
					{ 	
						userLocations.insert({
						    "source_name":result.formatted_address,
						    "location":{"lng":lng1,"lat":lat1},
						    "destination_name": null,
						    "location_destination":{"lng":0,"lat":0},
							"userId":Meteor.userId()
						});
					}
					else
					{
						console.log("update existing loc object");
						userLocations.update({ _id: obj._id}, {$set: {
							source_name: result.formatted_address,
							location: {"lng":lng1,"lat":lat1},
							"destination_name":null,
					 		"location_destination":{"lng":0,"lat":0} } 
						});

							 
					}

					$('[name=destination]').prop('disabled',false);
					$('[name=destination]').val("");
					$(".estimate").hide();


				}// else ends here
			
	        }); //end of geocomplete.bind


			$('#dest').geocomplete().bind("geocode:result",function(event, result){
		    	des_lat=result.geometry.location.lat();
	        	console.log("destination lat: "+des_lat);
	        	des_lng=result.geometry.location.lng();  
	        	console.log("destination lng: "+des_lng);  


	        
	        	reverseGeocode.getLocation(des_lat, des_lng, function(location){
					//var	destination_name=reverseGeocode.getAddrStr();	
					var	destination_name= result.formatted_address;			   	
			   		obj = userLocations.findOne({userId:Meteor.userId()});		
		        	
		        	userLocations.update({_id: obj._id}, {$set: {
		        		destination_name:destination_name,
		        		location_destination:{"lng":des_lng,"lat":des_lat}
		        	}});	

		        	$(".estimate").show();  
			   });// reverse geocode

	      	}); //#dest.geocomplete  
		} //Googlemaps.loaded
	});  //autorun
});
