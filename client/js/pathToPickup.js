/** 
* This js file contains the helpers and events for to assist 
* pathToPickup.html template 
* ********WE ARE NOT USING IT ANYMORE*******
**/

Template.pathToPickup.helpers({      
	    //this method returns the name of the rider whose request is accepted
  		riderName: function(){	
  			var user = Meteor.users.findOne(Session.get("riderid"));
  		  var firstName = user && user.profile && user.profile.name;
  		  return firstName;
  		},

     reachedPickUp: function(){
        return Session.get("reachedAtPickup");
     }

	});	 


Meteor.startup(function() {
    GoogleMaps.load();
});

Template.pathToPickup.events({
   
    'click #atPickUpLoc':function(event){

      // update the databse

      //set a reactive var
      
      Session.set("reachedAtPickup", "true");  
    },   

    'click #startTripBTn':function(event){

      // update the databse

      //set a reactive var
      
      Session.set("startTrip", "true");  

      // navigate to another page with new map (navigate to destination)


    },    


 });

 Template.pathToPickup.helpers({
   exampleMapOptions: function() {
   // Make sure the maps API has loaded

   var tripID = Session.get("tripID");

   
   var tripObj=Trips.findOne({_id:tripID});	

   //getting source coords of rider   
   userLocationsObj_rider = userLocations.findOne({userId:tripObj.rider_id});
   var lat_rider = userLocationsObj_rider.location.lat;
   var lng_rider = userLocationsObj_rider.location.lng;
    
   //getting source coords of driver 
   userLocationsObj_driver = userLocations.findOne({userId:tripObj.driver_id});
   var lat_driver = userLocationsObj_driver.location.lat;
   var lng_driver = userLocationsObj_driver.location.lng;


    if (GoogleMaps.loaded()) {
      // Map initialization options  

      return {
        
        center: new google.maps.LatLng(lat_rider,lng_rider),
        center2: new google.maps.LatLng(lat_driver,lng_driver),
        zoom: 12
      };
    }
  }
});


Template.pathToPickup.onCreated(function() {
  // We can use the `ready` callback to interact with the map API once the map is ready.
  GoogleMaps.ready('exampleMap', function(map) {
    // Add a marker to the map once it's ready
    var marker = new google.maps.Marker({
      position: map.options.center,
      map: map.instance
    });
    marker.setIcon('rider.png');
     var marker2 = new google.maps.Marker({
      position: map.options.center2,
      map: map.instance
    });
    marker2.setIcon('caricon.png');
  });
});
