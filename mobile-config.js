/**
* This file contains the configuration settings for mobile
**/

//This section set up some basic meta data
App.info({
	id: 'atrober-88648.onmodulus.net',
	name: 'latrober',
	description: 'Get to uni faster with latrober app',
	author: 'teamP latrobe',
	email: '1818587@students.latrobe.edu.au',
	website: 'http://latrober-88648.onmodulus.net'

});


// Set up resources such as icons and launch screen
/**App.icons({
	'android': 'logo-blue.xcf',
	'iphone': 'logo-blue.xcf'

});**/


App.accessRule('*');
App.accessRule('data:*', { type: 'navigation' });
App.accessRule('https://*.googleapis.com/*');
App.accessRule('https://*.google.com/*');
App.accessRule('https://*.gstatic.com/*');
App.configurePlugin('cordova-plugin-googlemaps', {
    'API_KEY_FOR_ANDROID': 'AIzaSyBk4Hc0kmrrZdawQSLVFjR0_KCuKcHNivo',
    'API_KEY_FOR_IOS': 'AIzaSyBoFWbgSwXcUKfPdH_ibFxLmyB5aSqVxkw'
});