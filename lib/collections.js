/*
This file contains the collections that are used in the app 
*/

/**
*Creating new collecting
**/
tripHistory=new Mongo.Collection("tripHistory");

userLocations=new Mongo.Collection("userLocations");

Trips = new Mongo.Collection("trips");

/**
* Allowing different rules to different collections 
**/


Trips.allow({
  insert:function(){
    
return true; 
  },
 remove:function(){
     return true;
   },
 update:function(){
     return true;
   } 
});


Meteor.users.allow({
	update:function(){
		return true;
	}
});

userLocations.allow({
	update:function(){
		return true;
	},
	insert:function(){
    	return true; 
    }
	
});


tripHistory.allow({
	update:function(){
		return true;
	},
	insert:function(){
    	return true; 
    }
	
});
