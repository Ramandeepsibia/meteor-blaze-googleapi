//Routes to templates
Router.configure({
  layoutTemplate: 'head'
});


Router.route('/register');
Router.route('welcome');
Router.route('forgotpassword');
/**Router.route('/', {
    template: 'landingPage'
});
**/

Router.route('/', {
    template: 'login'
});


/**Router.route('/', {
    template: 'map'
}); **/

Router.route('/profileTest', {
    template: 'profileTest'
});
Router.route('pathToPickup');

Router.route('/driverLocationModal');
Router.route('/vehicles');
//Router.route('/map');
Router.route('/myTemplate');
Router.route('/driverToPickUp');
Router.route('/driveToFinalDestination');
Router.route('/rateTrip');
Router.route('/rateingByDriver');
Router.route('/settingsPage');
Router.route('/becomeDriver');
Router.route('/isBoth');
Router.route('/tripHistoryModal');